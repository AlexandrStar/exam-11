const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
      await collection.drop();
  }

  const user = await User.create(
    {
      username: 'vasya1980',
      password: '1',
      display_name: 'Vasya',
      phone_number: 996555111111,
      token: '1',
    },
    {
      username: 'petya1973',
      password: '2',
      display_name: 'Petya',
      phone_number: 996555222222,
      token: '2',
    },
    {
      username: 'misha1981',
      password: '3',
      display_name: 'Misha',
      phone_number: 996555333333,
      token: '3',
    },
  );

  const [real_estate, cars, сomputers, hookah] = await Category.create(
    {title: 'Real estate'},
    {title: 'Cars'},
    {title: 'Computers'},
    {title: 'Hookah'},
  );

  await Product.create(
    {
      title: 'Дом 150кв.м',
      price: 5000000,
      description: 'Новый уютный дом',
      image: "house.jpg",
      category: real_estate._id,
      user: user[0]._id
    },
    {
      title: 'Дом 250кв.м',
      price: 15000000,
      description: 'Большой комфортный дом',
      image: "house_250.jpg",
      category: real_estate._id,
      user: user[1]._id
    },
    {
      title: 'Дом 350кв.м',
      price: 25000000,
      description: 'Большой комфортный дом',
      image: "house_350.jpg",
      category: real_estate._id,
      user: user[2]._id
    },
    {
      title: 'Volkswagen Multivan T6',
      price: 5000000,
      description: 'Лучший вэн в своем классе',
      image: "multivan.jpg",
      category: cars._id,
      user: user[2]._id
    },
    {
      title: 'MacBook Pro 2018 MR942 15.4',
      price: 185000,
      description: 'Макбук Apple MacBook Pro 2018 MR942 15.4' +
        ' Intel Core i7 8850H 16 Gb 512 Gb SSD Radeon Pro 560X серый космос',
      image: "macbook.jpg",
      category: сomputers._id,
      user: user[1]._id
    },
    {
      title: 'Hookah MattPear Simple 3D model',
      price: 25000,
      description: 'Hookah MattPear Simple M Black color',
      image: "hookah-mattpear.jpg",
      category: hookah._id,
      user: user[0]._id
    },

  );



  await connection.close();
};



run().catch(error => {
  console.log('Something went wrong', error);
});