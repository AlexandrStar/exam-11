const express = require('express');
const Category = require('../models/Category');
const Product = require('../models/Product');

const router = express.Router();

router.get('/', (req, res) => {
  Category.find().sort('_id')
    .then(categories => res.send(categories))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', async (req, res) => {
  try {
    const product = await Product.find({category: req.params.id}).populate('category').sort('_id');
    res.send(product);
  }catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
