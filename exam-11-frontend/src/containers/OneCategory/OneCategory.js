import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";
import {apiURL} from "../../constants";
import {connect} from "react-redux";
import {fetchOneCategory} from "../../store/actions/categoriesActions";
import {Link} from "react-router-dom";

class OneCategory extends Component {
  componentDidMount() {
    this.props.onFetchProduct(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        {this.props.oneCategory && this.props.oneCategory.map(category => (
          <Card key={category._id} style={{width: '640px', marginBottom: '25px'}} >
            <CardImg top style={{width: "100%"}} src={apiURL + '/uploads/' + category.image} alt="post" />
            <CardBody>
              <CardTitle>
                <Link to={'/products/' + category._id}>
                {category.title}
                </Link>
              </CardTitle>
              <CardText>{category.description}</CardText>
              <CardText><strong>{category.price} KGS</strong></CardText>
            </CardBody>
          </Card>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  oneCategory: state.categories.oneCategory
});

const mapDispatchToProps = dispatch => ({
  onFetchProduct: (category) => dispatch(fetchOneCategory(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneCategory);
