import React, {Component, Fragment} from 'react';
import {fetchProducts} from "../../store/actions/productsActions";
import {connect} from "react-redux";
import ProductListItem from "../../components/ProductListItem/ProductListItem";
import {Col, ListGroup, ListGroupItem, Row} from "reactstrap";
import {Link} from "react-router-dom";
import {fetchCategories} from "../../store/actions/categoriesActions";

class Products extends Component {
  componentDidMount() {
    this.props.onFetchProducts();
    this.props.onFetchCategories();
  }

  render() {
    return (
      <Fragment>
        <h2>
          Products
        </h2>
        <Row>
          <Col sm={{size: 'auto'}}>
          <ListGroup >
            {this.props.categories.map(category => (
              <ListGroupItem key={category._id}><Link to={'/categories/' + category._id}>
                {category.title}
              </Link></ListGroupItem>
            ))}
          </ListGroup>
          </Col>
          {this.props.products.map(product => (
            <ProductListItem
              key={product._id}
              _id={product._id}
              title={product.title}
              price={product.price}
              image={product.image}
            />
          ))}
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products.products,
  categories: state.categories.categories,
  oneCategory: state.categories.oneCategory
});

const mapDispatchToProps = dispatch => ({
  onFetchProducts: () => dispatch(fetchProducts()),
  onFetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
