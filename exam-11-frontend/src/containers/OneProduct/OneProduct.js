import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";
import {apiURL} from "../../constants";
import {connect} from "react-redux";
import {fetchOneProduct, removeProduct} from "../../store/actions/productsActions";

class OneProduct extends Component {
  componentDidMount() {
    this.props.onFetchProduct(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        {this.props.oneProduct ?
          <Card style={{width: '640px', marginBottom: '25px'}} >
            <CardBody>
              <CardText>{this.props.oneProduct.category.title}</CardText>
              <CardImg top style={{width: "100%"}} src={apiURL + '/uploads/' + this.props.oneProduct.image} alt="post" />
              <CardTitle>{this.props.oneProduct.title}</CardTitle>
              <CardText>{this.props.oneProduct.description}</CardText>
              <CardText><strong>Цена: {this.props.oneProduct.price} KGS</strong></CardText>
              <CardText>User: {this.props.oneProduct.user.display_name}</CardText>
              <CardText>Тел.: {this.props.oneProduct.user.phone_number}</CardText>
              {this.props.user._id === this.props.oneProduct.user._id ?
              <Button color="success" onClick={() => this.props.removeProduct(this.props.match.params.id)}>Продано</Button>: null}
            </CardBody>
          </Card>: null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  oneProduct: state.products.oneProduct,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchProduct: (product) => dispatch(fetchOneProduct(product)),
  removeProduct: (productData) => dispatch(removeProduct(productData))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneProduct);
