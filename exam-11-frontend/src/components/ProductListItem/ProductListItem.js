import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody, CardImg, CardText, Col} from "reactstrap";
import {Link} from "react-router-dom";
import {apiURL} from "../../constants";


const ProductListItem = props => {
  return (
  <Col sm={4}>
    <Card body style={{marginBottom: '10px'}}>
      <CardImg top style={{width: "100%", height: "80%"}} src={apiURL + '/uploads/' + props.image} alt="image" />
      <CardBody>
        <Link to={'/products/' + props._id}>
          {props.title}
        </Link>
        <CardText>
          <strong>
          {props.price} KGS
          </strong>
        </CardText>
      </CardBody>
    </Card>
  </Col>
  );
};

ProductListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

export default ProductListItem;
